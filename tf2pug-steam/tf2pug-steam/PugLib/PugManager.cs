﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SteamBot.Handlers;
using SteamBot.TF2PugAPI;
using SteamBot.ClanLib;

using SteamKit2;

namespace SteamBot.PugLib
{
    class PugManager
    {
        private SteamFriends steam_friends;
        private ClanManager clan_manager;

        private List<Pug> pug_list;

        private TF2PugInterface api;

        public PugManager(SteamFriends steam_friends, ClanManager clan_manager)
        {
            api = new TF2PugInterface();

            this.pug_list = new List<Pug>();
            this.steam_friends = steam_friends;
            this.clan_manager = clan_manager;

            GetPugListing();
        }

        /**
         * Gets an up to date pug listing from the API
         */
        public void GetPugListing()
        {
            Program.log.Info("PUG LISTING");
            ResultContainer result = api.GetPugListing();
            Program.log.Debug("RESPONSE: {0}", result.response);
            

            // if the code is a pug listing response, we need to update our
            // list
            if (result.response == EPugAPIResponse.Response_PugListing)
            {
                pug_list.Clear();

                foreach (var pug in result.Data["pugs"])
                    HydratePug(pug);

                //HydrateAll(result.Data["pugs"]);
            }
        }

        /**
         * Attempts to add a player to a pug via the API. The response is as
         * document in the TF2Pug API documentation.
         */
        public void AddPlayer(SteamID player, int pug_id = -1)
        {
            string player_name = steam_friends.GetFriendPersonaName(player);
            ResultContainer result = api.AddPlayer(player.ConvertToUInt64(), player_name, pug_id);

            if (result.response == EPugAPIResponse.Response_PlayerAdded)
            {
                // we know that there's only 1 pug returned with this response code
                Pug pug = HydratePug(result.Data["pug"]);

                String msg = String.Format("{0} has been added to pug {1}. ({2})",
                        player_name, pug.id, pug.SlotsRemaining);

                ChatHandler.sendMainRoomMessage(msg);

                // we should check to see if the pug state is now map voting
                // i.e the pug is full
                if (pug.Full)
                {
                    if (pug.state == EPugState.MAP_VOTING)
                    {
                        // pug is now in map voting
                        msg = String.Format("Pug {0} is full. Map voting is now in progress",
                                pug.id
                            );
                        ChatHandler.sendMainRoomMessage(msg);

                        msg = String.Format("To vote for a map, type !map <map>. eg, !map cp_granary. Available maps: {0}",
                                pug.Maps
                            );
                        ChatHandler.sendMainRoomMessage(msg);

                        // we should setup a timer, or async callback, that
                        // will trigger at map_vote_end and output the winning
                        // map and send details
                    }
                    else if (pug.state == EPugState.MAPVOTE_COMPLETE)
                    {
                        // pug has the map forced, so we ignore map voting and
                        // go straight to details
                        msg = String.Format("Pug {0} is full. Map is {1}. Please join the server promptly",
                                pug.id, pug.map
                            );
                        ChatHandler.sendMainRoomMessage(msg);

                        SendMassDetails(pug);
                    }
                    else if (pug.state == EPugState.GAME_STARTED)
                    {
                        // person has joined as a replacement, send them the details
                        Details(player, pug);
                    }
                }
            }
            else if (result.response == EPugAPIResponse.Response_PlayerInPug)
            {
                ChatHandler.sendMessage(null, player, "You're already in a pug");
            }
            else if (result.response == EPugAPIResponse.Response_PugFull)
            {
                ChatHandler.sendMessage(null, player, "Pug is full");
                GetPugListing();
            }
            else if (result.response == EPugAPIResponse.Response_InvalidPug)
            {
                ChatHandler.sendMessage(null, player, "That pug does not exist");
                GetPugListing();
            }
            else
            {
                Program.log.Error("@PUGMANAGER Unexpected response {0} when adding player: ", result.response);
            }
        }

        public void RemovePlayer(SteamID player)
        {
            ResultContainer result = api.RemovePlayer(player.ConvertToUInt64());

            if (result.response == EPugAPIResponse.Response_PlayerRemoved)
            {
                Pug pug = HydratePug(result.Data["pug"]);

                String msg = String.Format("{0} has been removed from pug {1}. ({2})",
                        steam_friends.GetFriendPersonaName(player), pug.id, pug.SlotsRemaining
                    );

                ChatHandler.sendMainRoomMessage(msg);
            }
            else if (result.response == EPugAPIResponse.Response_PlayerNotInPug)
            {
                GetPugListing();
            }
            else if (result.response == EPugAPIResponse.Response_EmptyPugEnded)
            {
                String msg = String.Format("{0} has been removed. Empty pug has been ended",
                    steam_friends.GetFriendPersonaName(player));

                ChatHandler.sendMainRoomMessage(msg);
                // we know that a pug just ended, so we should get an updated listing
                GetPugListing();
            }
            else
            {
                Program.log.Error("@PUGMANAGER Unexpected response {0} when removing player: ", result.response);
            }
        }

        public void CreateNewPug(SteamID user, int size = 12)
        {
            string player_name = steam_friends.GetFriendPersonaName(user);
            ResultContainer result = api.CreatePug(
                    user.ConvertToUInt64(), player_name, size
                );

            if (result.response == EPugAPIResponse.Response_PlayerInPug)
            {
                ChatHandler.sendMessage(null, user, "You're already in a pug");
            }
            else if (result.response == EPugAPIResponse.Response_PugCreated)
            {
                Pug pug = HydratePug(result.Data["pug"]);

                String msg = String.Format("A {0} player pug has been started by {1}. Type !j to join",
                        pug.size, player_name
                    );

                ChatHandler.sendMainRoomMessage(msg);
            }
            else
            {
                Program.log.Error("@PUGMANAGER Unexpected response {0} when creating pug: ", result.response);
            }
        }

        /**
         * Wraps EndPug(Pug) by getting the user's pug
         */
        public void EndPug(SteamID user, bool admin)
        {
            // Update our listing first, because the user may have joined
            // via another service since the last update
            GetPugListing();

            Pug pug = GetPlayerPug(user);

            if (pug != null && (user == pug.admin.SID || admin))
            {
                EndPug(pug);
            }
        }

        public void EndPug(Pug pug)
        {
            ResultContainer result = api.EndPug(pug.id);

            if (result.response == EPugAPIResponse.Response_PugEnded)
            {
                // pug has been ended. we should update our listing
                GetPugListing();

                String msg = String.Format("Pug {0} has been ended", pug.id);
                ChatHandler.sendMainRoomMessage(msg);
            }
            else if (result.response == EPugAPIResponse.Response_InvalidPug)
            {
                // Somehow we specified an invalid pug. Get an up-to-date listing
                GetPugListing();
            }
            else
            {
                Program.log.Error("@PUGMANAGER Unexpected response {0} when creating pug: ", result.response);
            }
        }

        public void VoteMap(SteamID user, string map)
        {
            ResultContainer result = api.AddMapVote(user.ConvertToUInt64(), map);

            if (result.response == EPugAPIResponse.Response_MapVoteAdded)
            {
                Pug pug = HydratePug(result.Data["pug"]);

                String msg = String.Format("{0} voted for {1} (Total: {2})",
                        steam_friends.GetFriendPersonaName(user), map, pug.MapVoteCount(map)
                    );
                
                ChatHandler.sendMainRoomMessage(msg);
            }
            else if (result.response == EPugAPIResponse.Response_InvalidMap)
            {
                return;
            }
            else if (result.response == EPugAPIResponse.Response_PlayerNotInPug)
            {
                return;
            }
            else if (result.response == EPugAPIResponse.Response_MapVoteNotInProgress)
            {
                return;
            }
            else
            {
                Program.log.Error("@PUGMANAGER Unexpected response {0} when adding map vote: ", result.response);
            }
        }

        public void ForceMapVote(Pug pug)
        {
            return;
        }

        public void ForceMapVote(SteamID sender)
        {
            return;
        }

        void SendMassDetails(Pug pug)
        {
            foreach (var player in pug.Players)
            {
                EPersonaState player_state = steam_friends.GetFriendPersonaState(player.SID);
                if (player_state != EPersonaState.Offline)
                {
                    Details(player.SID, pug);
                }
            }
        }

        public void Details(SteamID player, Pug pug)
        {
            if (pug == null)
                return;

            String msg = String.Format("Details for pug {0}: {1}",
                    pug.id, pug.SteamConnectString
                );
            ChatHandler.sendMessage(null, player, msg);
        }

        /**
         * Hydrates all the pugs in the given pugs list.
         */
        void HydrateAll(dynamic pugs)
        {
            foreach (var pug in pugs)
            {
                HydratePug(pug);
            }
        }

        /**
         * Hydrates a Pug object with the data available in the JSON response
         * 
         * @return Pug Hydrated pug object
         */
        Pug HydratePug(dynamic pug_json)
        {
            Pug pug = new Pug();

            pug.id = pug_json["id"];
            pug.state = (EPugState)pug_json["state"];
            pug.size = pug_json["size"];

            pug.map_vote_start = pug_json["map_vote_start"];
            pug.map_vote_end = pug_json["map_vote_end"];
            pug.map = pug_json["map"];
            pug.map_forced = pug_json["map_forced"];

            foreach (var mapvote in pug_json["player_votes"])
            {
                pug.AddMapVote((long)mapvote["id"], (string)mapvote["map"]);
            }

            foreach (var votecount in pug_json["map_vote_counts"])
            {
                //Console.WriteLine(votecount["map"].GetType());
                //Console.WriteLine(votecount["count"].GetType());
                pug.SetMapVoteCount((string)votecount["map"], (int)votecount["count"]);
            }

            foreach (var map in pug_json["maps"])
            {
                pug.maps.Add((string)map);
            }

            pug.mumble = pug_json["mumble"];

            pug.ip = pug_json["ip"];
            pug.port = pug_json["port"];
            pug.password = pug_json["password"];

            // now we need to handle players and teams
            foreach (var player in pug_json["players"])
            {
                Player new_player = new Player((ulong)player["id"], (string)player["name"]);

                if (player["id"] == pug_json["admin"])
                    pug.admin = new_player;

                pug.Add(new_player);
            }

            foreach (var member in pug_json["team_red"])
            {
                pug.AddRedMember((long)member);
            }

            foreach (var member in pug_json["team_blue"])
            {
                pug.AddBlueMember((long)member);
            }


            AddOrReplaceHydratedPug(pug);

            return pug;
        }

        /**
         * Checks if a pug with the same id exists and overwrites it if it 
         * does. If it doesn't exist, it will be added to the list.
         * 
         * @param Pug The pug to replace (or add)
         */
        void AddOrReplaceHydratedPug(Pug pug)
        {
            Program.log.Info("Adding/replacing hydrated pug {0}", pug.id);
            for (int i = 0; i < pug_list.Count; i++)
            {
                if (pug_list[i].id == pug.id)
                {
                    pug_list[i] = pug;

                    return;
                }
            }

            pug_list.Add(pug);
        }

        public Pug GetPugById(long id)
        {
            return pug_list.Find(pug => pug.id == id);
        }

        public Pug GetPlayerPug(SteamID player)
        {
            return pug_list.Find(pug => (pug.Players.Find(p => p.SID == player) != null));
        }

        public List<Pug> Pugs
        {
            get { return this.pug_list; }
        }

        public String GetPugPlayerListAsString(Pug pug)
        {
            return String.Join(", ", GetPugPlayerNames(pug));
        }

        public List<String> GetPugPlayerNames(Pug pug)
        {
            return pug.Players.Select(player => player.Name).ToList();
        }

        /** 
         * Gets the current unix timestamp with respect to UTC time
         * 
         * @return long Unix timestamp (in seconds)
         */
        public static long GetUnixTimeStamp()
        {
            return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}
