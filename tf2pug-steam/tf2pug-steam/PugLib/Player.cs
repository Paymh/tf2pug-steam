﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SteamKit2;

namespace SteamBot.PugLib
{
    class Player
    {
        private SteamID steamid;
        private string name;

        public Player(ulong steamid, string name)
        {
            this.steamid = new SteamID(steamid);
            this.name = name;
        }

        public Player(SteamID id, string name)
        {
            this.steamid = id;
            this.name = name;
        }

        public string Name
        {
            get { return this.name; }
        }

        public SteamID SID
        {
            get { return this.steamid; }
        }
    }
}
